<!DOCTYPE html>
<head>
    
    <title>UMEXPERT - PROFESOR DR. ABRIZAH BINTI ABDULLAH</title>



    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
   
   
     <link rel="stylesheet" href="css/highcharts.css">
    <link rel="stylesheet" href="css/dashboard.css">


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/highcharts.js"></script>



    <script type="text/javascript">
    $(document).ready(function() {



        lineChart();

function lineChart() {
        $("#containerGraph").highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Annual Publications by Year'
                },
                xAxis: {
                    categories: [2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017],
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Number of Publications'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Journal',
                    data: [0, 28, 25, 20, 28, 28, 47, 79, 72, 45, 118, 190, 198, 115, 0]
                }, {
                    name: 'SCOPUS',
                    data: [0, 25, 141, 164, 130, 255, 240, 230, 242, 256, 239, 230, 120, 149, 58]
                }, {
                    name: 'Other Indexing',
                    data: [25, 60, 40, 25, 160, 180, 176, 100, 262, 241, 205, 170, 110, 58, 0]
                }, {
                    name: 'Book',
                    data: [0, 0, 10, 19, 11, 2, 22, 2, 9, 6, 1, 5, 17, 0, 27]
                }, {
                    name: 'Chapter in Book',
                    data: [8, 8, 5, 18, 9, 16, 4, 1, 0, 2, 41, 5, 9, 7, 2]
                }]
            }


        ); //end of chart container
    }

    }); // end of documetn


    
    </script>
</head>

<body>
    <header class="header-part">
        <div style="background-color:#428bca;color:#fff;line-height:18Px;padding:3px 0;font-size:12Px !important;">
            <div class="" style="padding:  0 17px ">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="pull-left" style="padding-right:20px; font-weight: bold;">
                            <i class="fa fa-phone"></i>+91 8000 356 357
                        </div>
                        <div class="pull-left" style="font-weight: bold;">
                            <i class="fa fa-envelope"></i><a href="mailto:info@keyconcepts.co.in" style="color:#fff;"> info@keyconcepts.co.in</a>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="pull-right">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="https://www.facebook.com/KeyConceptsITServicesLLP/" target="_blank"><i class="fa fa-facebook social-icons" style="color:#fff;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="https://twitter.com/kcitsindia" target="_blank"><i class="fa fa-twitter social-icons" style="color:#fff;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="https://plus.google.com/+KeyconceptsCoIn/posts" target="_blank"><i class="fa fa-google-plus social-icons" style="color:#fff;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="https://in.linkedin.com/in/keyconceptsindia" target="_blank"><i class="fa fa-linkedin social-icons" style="color:#fff;"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="Div1" class="wrapper">
            <!-- Fixed navbar -->
            <div class="navi navbar-default" role="navigation">
                <div class="" style="padding:  0 17px ">
                    <div class="row">
                        

                   
                    <div  class="col-lg-4 col-md-4 col-sm-12" style="margin-top: 5px;">
                        <a href="index.php"><span class="text-size-small text-bold">HOME</span></a> / 
                        <a href="index.html"><span class="text-size-small text-bold">PROFILE</span></a> / 
                         <a href="index.html"><span class="text-size-small text-bold">PUBLICATION</span></a>

                    </div>
                    <div  class="col-lg-8 col-md-8 col-sm-12">
                       <!--  <marquee behavior="alternate" style="" direction="right" scrollamount="2" loop="-1">
                                            <center>
                                              <blink>Welcome To Professor Dato' Dr. Hassan Said
                                                  personal Web page</blink> 
                                            </center>
                                          </marquee> -->
                    </div>
                     
                    <div style="clear:both;"></div>
                     </div>
                </div>
            </div>
            <!-- End of Nav -->
        </div>
    </header>
    <!--Start Content-->
    <div class="" style="padding:10px 0 0 0;">
        <div id="" class="col-xs-12 col-sm-12">
            <!--Start Dashboard Content-->
            <div class="row">
                <!-- Middle Content [Left]-->
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <div class="panel panel-overflow  ">
                        <div class="profile-upper profile-background-image" style="border-top: 3px solid #428bca" >
                           <!--  <div>
                                <img src="https://bootstrap-themes.github.io/application/assets/img/instagram_13.jpg">
                            </div> -->
                            <img src="http://portal.um.edu.my/ihris/gambar_staf/00005631.jpg" alt="avatar" class="img-circle img-thumbnail img-thumbnail-profile-2x">

                            <h3 class="profile-title">Prof. Dr. Abrizah Binti Abdullah</h3>
                            <h4 class="text-size-small">Department Of Library Science & Information</h4>
                            <h4 class="text-size-small">Faculty Of Computer Science & Information Technology</h4>
                            <span class="text-muted text-size-small f-w-500">abrizah@um.edu.my</span>
                        </div>
                        <div class="profile-footer" style="padding: 0px">
                            <table class="table table-hover personal-info">
                                <tbody>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-tachometer"></i></td>
                                        <td style="width: 80%; border-right: 2px solid #428BCA">
                                            <a href="index.php"><span class="text-size-small text-bold">HOME</span></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-user"></i></td>
                                        <td style="width: 80%"><a href="#"><span class="text-size-small text-bold">PROFILE</span></a></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-tasks"></i></td>
                                        <td style="width: 80%"><a href="#"><span class="text-size-small text-bold">PUBLICATIONS</span></a></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-briefcase"></i></td>
                                        <td style="width: 80%"><a href="#"><span class="text-size-small text-bold">WORK EXPERIENCE</span></a></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-graduation-cap"></i></td>
                                        <td style="width: 80%"><a href="#"><span class="text-size-small text-bold">EDUCATION</span></a></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-phone"></i></td>
                                        <td style="width: 80%"><a href="#"><span class="text-size-small text-bold">CONTACT</span></a></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class="fa fa-edit"></i></td>
                                        <td style="width: 80%"><a href="#" target="_blank"><span class="text-size-small text-bold">VIEW CV</span></a></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: center;"><i class=" fa fa-info"></i></td>
                                        <td style="width: 80%">
                                            <a data-toggle="collapse" href="#collapse1" class="text-size-small text-bold">BIOGRAPHY</a>
                                        </td>
                                    </tr>
                                    <tr style="margin:-3em;">
                                        <td colspan="2">
                                            <div id="collapse1" class="well panel-collapse collapse m-0 f-12 text-justify">
                                                <p class="MsoNormal" style="text-align: justify; line-height: normal; margin: 0in 0in 0pt; mso-layout-grid-align: none;">
                                                    <span style="font-family: Calibri;">
                                            <span style="font-size: 12px; mso-bidi-font-family: AdvPS405B6;">
                                         Abrizah Abdullah is a Professor at the Department of Library &amp; Information Science University of Malaya, Kuala Lumpur. She was a teacher librarian, and started her teaching career in 1990; and was then appointed as an Assistant Director at the Educational Planning and Research Division of the Ministry of Education Malaysia before joining the University in 2000. She graduated with a bachelor degree in Environmental Engineering from Temple University, Philadelphia in 1988, and obtained her Masters degree in 1998 and PhD in 2007, both in Library &amp; information Science, from the University of Malaya. Her research interests focus on digital libraries, Open Access Initiatives, information behaviour, bibliometrics and scholarly communication. She is the chief editor of the 
                                        

                                         Malaysian Journal of

                                        Library &amp; Information Science (SSCI) and a research fellow at the Malaysian Citation Centre Ministry of Higher Education Malaysia. She is currently the Dean of Insitute of Graduate Studies, University of Malaya.
                                         </span>
                                                    </span>
                                                </p>
                                                <br />
                                                <div class="row text-center">
                                                    <a data-toggle="collapse" href="#collapse1" class="btn btn-info btn-xs"><i class="fa fa-times" aria-hidden="true"></i> <span>Close</span></a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel panel-overflow">
                        <!--<div class="panel panel-border-top widget-publication">-->
                        <div class="panel-heading widget-h bl-default">
                            <span>Bibliometric Data</span>
                        </div>
                        <div class="panel-body " style="padding-top: 0px; padding-bottom: 0px;">
                            <table class="table table-hover personal-info">
                                <tbody>
                                    <tr>
                                        <td width="5%"><i class="fa fa-paperclip"></i></td>
                                        <td><span class="text-size-small text-bold">Total Articles in Publication List</span></td>
                                        <td width="10%"><span class="text-size-small text-bold">52</span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-paperclip"></i></td>
                                        <td><span class="text-size-small text-bold">Articles With Citation Data</span></td>
                                        <td><span class="text-size-small text-bold">52</span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-paperclip"></i></td>
                                        <td><span class="text-size-small text-bold">Sum of the Times Cited</span></td>
                                        <td><span class="text-size-small text-bold">174</span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-paperclip"></i></td>
                                        <td><span class="text-size-small text-bold">Average Citations per Article</span></td>
                                        <td><span class="text-size-small text-bold">3.35</span></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-paperclip"></i></td>
                                        <td><span class="text-size-small text-bold">H-Index</span></td>
                                        <td><span class="text-size-small text-bold">7</span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <link rel="stylesheet" href="//umexpert.um.edu.my/plugins/overlay-bootstrap/overlay-bootstrap.min.css">
                    <link rel="stylesheet" href="//umexpert.um.edu.my/plugins/overlay-bootstrap/overlay-bootstrap.css">
                    <link rel="stylesheet" href="//umexpert.um.edu.my/plugins/jquery-plugin-circliful/css/jquery.circliful.css" type="text/css" />
                    <script src="//umexpert.um.edu.my/plugins/jquery-plugin-circliful/js/jquery.circliful.min.js"></script>
                    <script src="//umexpert.um.edu.my/js/bootstrap-tabdrop.js"></script>
                    <script type="text/javascript">
                    $(document).ready(function() {
                        $("#awardstat").circliful({
                            animation: true,
                            foregroundBorderWidth: 5,
                            backgroundBorderWidth: 5,
                            foregroundColor: '#02AFA5',
                            backgroundColor: '#eee',
                            icon: "f091",
                            iconSize: 62,
                            iconPosition: "bottom",
                            iconColor: '#02AFA5',
                            text: ""
                        });
                    });
                    </script>
                    <div class="row">
                        <!-- Start Widget Awards -->
                        <div class="col-xs-6 col-sm-4 col-md-6 col-lg-6">
                            <div class="thumbnail text-center m-b-25">
                                <div class="panel-body widget-awards">
                                    <div class="widget-top">
                                        <span class="widget-title">Latest Award</span>
                                    </div>
                                    <div class="circle-container">
                                        <!-- Award icon -->
                                        <div id="awardstat"></div>
                                        <!-- /Award icon -->
                                    </div>
                                    <div class="caption caption-down">
                                        <span class="text-size-small f-w-500">Anugerah perkhidmatan cemerlang</span>
                                    </div>
                                    <div class="widget-awards-content">
                                        <span class="mt-15 mb-5 widget-label elipsis-2-line">
                        Anugerah perkhidmatan cemerlang                 </span>
                                        <div class="text-size-small text-muted f-w-500">Year 2015</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Widget Awards -->
                        <link rel="stylesheet" href="//umexpert.um.edu.my/plugins/overlay-bootstrap/overlay-bootstrap.min.css">
                        <link rel="stylesheet" href="//umexpert.um.edu.my/plugins/overlay-bootstrap/overlay-bootstrap.css">
                        <link rel="stylesheet" href="//umexpert.um.edu.my/plugins/jquery-plugin-circliful/css/jquery.circliful.css" type="text/css" />
                        <script src="//umexpert.um.edu.my/plugins/jquery-plugin-circliful/js/jquery.circliful.min.js"></script>
                        <script src="//umexpert.um.edu.my/js/bootstrap-tabdrop.js"></script>
                        <script type="text/javascript">
                        $(document).ready(function() {
                            $("#activitystat").circliful({
                                animation: true,
                                foregroundBorderWidth: 5,
                                backgroundBorderWidth: 5,
                                foregroundColor: '#E295AE',
                                backgroundColor: '#eee',
                                icon: "f087",
                                iconSize: 62,
                                iconPosition: "bottom",
                                iconColor: '#DF94AC',
                                text: ""
                            });
                        });
                        </script>
                        <!-- Start Widget Evaluation -->
                        <div class="col-xs-6 col-sm-4 col-md-6 col-lg-6">
                            <div class="thumbnail text-center m-b-25 activitystat">
                                <div class="panel-body widget-activity">
                                    <div class="widget-top">
                                        <span class="widget-title">Latest Evaluation</span>
                                    </div>
                                    <div class="circle-container">
                                        <!-- Activity icon -->
                                        <div id="activitystat"></div>
                                        <!-- /Activity icon -->
                                    </div>
                                    <div class="caption caption-down">
                                        <span class="text-size-small f-w-500">Scientometrics (SSCI - Q1)Scientometrics (SSCI - Q1)</span>
                                    </div>
                                    <div class="widget-activity-content">
                                        <span class="mt-15 mb-5 widget-label elipsis-2-line">
                        Reviewer                    </span>
                                        <div class="text-size-small text-muted f-w-500">Year 2013 - 2015</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Widget Evaluation -->
                    </div>
                </div>
                <!-- End Middle Content [Left]-->
                <!-- Middle Content [Right] -->
                <div class="col-md-8 col-sm-12 p-0">
                    <!-- Start Widget Statistics -->
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="panel widget-statistics">
                            <!-- <div class="panel-heading widget-h bl-default">
                                <span>Publication Statistics</span>
                                 <ul class="nav panel-tabs stat-tabs">
                                    <li class="active"><a href="#publication" data-toggle="tab" data-identifier="statPublication">Publication</a></li>
                                </ul> 
                            </div> -->
                            <div class="panel-body p-b-10 p-t-10">
                                <div class="tab-content p-0" >
                                 
                                <div id="containerGraph"  ></div>
                                </div>
                            </div>
                            <!-- / widget content -->
                        </div>
                        <!-- / .panel -->
                    </div>

                    
                </div>
                <!-- End Middle Content [Right] -->
            </div>
            <!--End Dashboard Content-->
        </div>
    </div>







    <div style="background-color:#333333;color:#fff;line-height:50Px;padding:3px 0;font-size:12Px !important;">
            <div class="" style="padding:  0 17px ">
                <div class="row">
                    <div class="col-lg-6 col-sm-10 col-xs-10">
                         Copyright © webcomindia.biz., 1999-2016

                        <div class="clear"></div>
                    </div>
                    <div class="col-lg-6 col-sm-2 col-xs-2">
                        <div class="pull-right">
                            <a href="#"><i class="fa fa-arrow-circle-up" style="color:#fff; font-size: 20px;"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                           
                        </div>
                    </div>
                </div>
            </div>
        </div>



</body>
</html>