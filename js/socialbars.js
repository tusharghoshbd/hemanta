$(document).ready(function() {
	
	// ["Label" , "website link" , "bar color" , "bar image"]
	var social = [
	
	
	 ["GoogleScholar", 	"/*http://scholar.google.ca/citations?user=oG89PhIAAAAJ&amp;hl=en*/", 			"#3B5998", "img/facebook.png"],
	
	 ["Google+", 	"/*https://plus.google.com/113061241513149365571/posts*/", 		"#dd4b39", "img/google_plus.png"],
	  ["ResearchGate", 	"/*http://www.researchgate.net/profile/Mohamed_Cheriet*/", 	"#5ba525", "img/evernote.png"],

	 ["Linkedin", 	"/*https://www.linkedin.com/pub/mohamed-cheriet/6/407/332*/","#0e76a8", "img/linkedin.png"],
	
	 ];

////////////////////////////////////////////////	
//// DO NOT EDIT ANYTHING BELOW THIS LINE! /////
////////////////////////////////////////////////
		
	$("#socialside").append('<ul class="mainul"></ul>');
	
	/// generating bars
	for(var i=0;i<social.length;i++){
	$(".mainul").append("<li>" + '<ul class="scli" style="background-color:' + social[i][2] + '">' +
						'<li>' + social[i][0] + '<img src="' + social[i][3] + '"/></li></ul></li>');
	 				}
	
	/// bar click event
	$(".scli").click(function(){
		var link = $(this).text();		
		for(var i=0;i<social.length;i++){
			if(social[i][0] == link){
				window.open(social[i][1]);
			}
		}		
	});
	
	/// mouse hover event
	$(".scli").mouseenter(function() {	
		$(this).stop(true);	
		$(this).clearQueue();
			$(this).animate({
				left : "140px"
			}, 300);
				
	});

	/// mouse out event
	$(".scli").mouseleave(function(){
		$(this).animate({
			left:"0px"
		},700);
	});

});
